import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ExecutionTest {

    @Test
    public void checkThisTest1() {
        System.out.println("1st test");
    }

    @Test
    public void checkThisTest2() {
        System.out.println("2nd test");
    }

    @Test
    public void checkThisTest3() {
        System.out.println("3rd test");
    }

    @Test
    public void checkThisTest4() {
        System.out.println("4h test");
    }
}
